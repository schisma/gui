module Data.Instrument where

import Prelude
import Data.Array (catMaybes)
import Data.Array as Array
import Data.Array.NonEmpty (NonEmptyArray, find, head)
import Data.Int (floor)
import Data.Map as Map
import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(..))
import Data.UUID (UUID)
import Foreign.Object (Object, fromFoldable, toUnfoldable)

import Data.Midi (MidiMessage, midiMessage)
import Data.Synth (Synth, SynthParameter, updateSynthParameters)
import Data.Utilities (scale)

type Instrument
  = { availableSynths :: NonEmptyArray Synth
    , id :: UUID
    , midiChannel :: Int
    , name :: String
    , soundFontPath :: String
    , synth :: Synth
    }

type InstrumentJson
  = { instrument :: String
    , midiChannel :: Int
    , name :: String
    , parameters :: Object Number
    , soundFontPath :: String
    }

fromInstrumentJson
  :: NonEmptyArray Synth
  -> UUID
  -> InstrumentJson
  -> Instrument
fromInstrumentJson availableSynths id json =
  let synth = case find (\s -> s.name == json.instrument) availableSynths of
                        Nothing -> head availableSynths
                        Just s -> s

      parameters = Map.fromFoldable $ (toUnfoldable json.parameters :: Array _)

  in  { availableSynths: availableSynths
      , id
      , midiChannel: json.midiChannel
      , name: json.name
      , soundFontPath: json.soundFontPath
      , synth: updateSynthParameters synth parameters
      }

toInstrumentJson :: Instrument -> InstrumentJson
toInstrumentJson instrument =
  { instrument: instrument.synth.name
  , midiChannel: instrument.midiChannel
  , name: instrument.name
  , parameters: fromFoldable $
                map (\p -> Tuple p.name p.value) instrument.synth.parameters
  , soundFontPath: instrument.soundFontPath
  }

midiControlChangeMessage :: Instrument -> SynthParameter -> Maybe MidiMessage
midiControlChangeMessage instrument synthParameter =
  case Array.find ((==) synthParameter) instrument.synth.parameters of
    Nothing -> Nothing
    Just parameter ->
      let value = floor $
                  scale
                  parameter.value
                  parameter.minimum
                  parameter.maximum
                  0.0
                  127.0
          channel = instrument.midiChannel
      in  Just $ midiMessage 176 parameter.midiCcNumber value channel

midiControlChangeMessages :: Instrument -> Array MidiMessage
midiControlChangeMessages instrument =
  catMaybes $
    map (midiControlChangeMessage instrument) instrument.synth.parameters

new :: NonEmptyArray Synth -> UUID -> Instrument
new availableSynths id =
  { availableSynths
  , id
  , midiChannel: 0
  , name: "New Instrument"
  , soundFontPath: ""
  , synth: head availableSynths
  }

remove :: Array Instrument -> Instrument -> Array Instrument
remove instruments instrumentToRemove =
  Array.filter ((/=) instrumentToRemove) instruments

updateSynth :: Instrument -> String -> Instrument
updateSynth instrument synthName =
  case find (\synth -> synth.name == synthName) instrument.availableSynths of
    Nothing -> instrument
    Just synth -> instrument { synth = synth }

updateSynthParameterValue :: Instrument -> SynthParameter -> Instrument
updateSynthParameterValue instrument synthParameter =
  let parameter = Map.singleton synthParameter.name synthParameter.value
      synth = updateSynthParameters instrument.synth parameter
  in  instrument { synth = synth }
